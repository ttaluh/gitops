# Clean ingress router
In haproxy we need to add the 2nd backend for the http and https traffic and add an acl into the front end
   
    acl clean hdr_sub(host) -i clean.bruce.openshift.local
    use_backend ocp4_https_ingress_traffic_clean_be is clean


Any namespace you wish to use for the clean route will need a namespace label to match that specified in the ingress router.

```
    namespaceSelector:
      matchLabels:
        isclean: "yes"
```


Then create a new route
```
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  labels:
    app: httpd
    app.kubernetes.io/component: httpd
    app.kubernetes.io/instance: httpd
    type: clean
  name: httpd
  namespace: testing
spec:
  host: httpd-testing.clean.bruce.openshift.local
  port:
    targetPort: 8080-tcp
  to:
    kind: Service
    name: httpd
    weight: 100
  wildcardPolicy: None

```
