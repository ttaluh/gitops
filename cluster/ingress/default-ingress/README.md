# Mods to default ingress 

- Added custom certs from the custom-certs-default secret
- Added node placement on infra nodes
- Added tolerations for infra nodes
- Set tlsSecurityProfile to Modern
