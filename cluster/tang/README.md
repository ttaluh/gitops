# Deploy Tangd Socket

## Problems

Ideally we would have 3 servers (one in each datacentre) but alas nothing is ever simple. We do thankfully have the two bastion servers in 2 of our datacentres so our problem is London.

Next we have the chicken/egg problem.

We intend on running the 3rd tang server in the cluster, **but** we need the tang server ready before we build the cluster since the details of each tang server is needed for the cluster bootstrap files.

So we need to know what the url for and the keys held in our tang pod from within the cluster before we even have a cluster. Marty McFly are you busy?

## The Horrible Solution

This is ***a*** way we can achieve what we need without time travel. This solution also ensures that the keys always remain in a trusted environment.

- Build the tangd container image in our 3.11 cluster
- create a statefulset from the image
- Once the pod is running we copy the generated keys out from /var/db/tang
- We retrieve the thumbprint for the keys.
- Using this info we can populate the bootstrap files for our OCP4 cluster.
- After the masters we bootstrap the the node in London
- Provision our tang statefulset
- Replace the keys with those we copied from our 3.11 cluster.
- Boostrap the remaining nodes.


## Notes about this solution

### Statefulset not Pod

It turns out that the tangs socket cares about the hosts identity in some way. I couldn't identify what specific thing but I suspect the hostname. Using stateful sets will work because we know the hostname will stay consistent i.e. tang-0. In previous experiment using the tang image in a pod after copying the keys into the pod tang would ignore them and recreate new ones, far from desirable behaviour. 

### Deploying nodes with only 2 of the 3 tang sockets available?

Testing show this works thanks to the threshold option in the luks config


```
      luks:
        - name: local1
          device: /dev/md/data
          clevis:
            tang:
              - url: http://tang1.badger.local:8080
                thumbprint: 38taIucDlp1XENfAOTsGn6wOnAs
              - url: http://tang2.badger.local:8080
                thumbprint: H-kFHZoqK5RFHaJjLeJFfTWuNs4
              - url: http://tang3.badger.local:8081
                thumbprint: RcHUfeAnpxbw-Fs7hD7g5RlUhc8
            threshold: 2 
          options: [--cipher, aes-cbc-essiv:sha256]
          wipeVolume: true
```
Here we set the threshold option to 2, which means that at least 2 of the 3 tang servers need to be available to unlock the drives. This means that a remote server will always be required to unlock the drives.




