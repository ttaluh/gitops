# Notes

## registry.yaml

Mods made
- added tolerations to allow registry pods to run on infra nodes
- set storage to emptydir (testing only in prod this will be a pv)o
- set defaultRoute: true - this causes the operator to add a route for external access to the reg (we're going to add our own so we could put this back to false)

## external-route.yaml

Add a route to the internal registry 
- with a friendlier name than the default
- with a custom cert
