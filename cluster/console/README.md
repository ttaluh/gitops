# Mods to console

## cli-downloads.yaml
In the console under "help" > "Command Line tools" 

This adds links the cli tools hosted on a local webserver. Handy when the internet ones aren't available.

## console-links

A directory with links to add to the console. Here we're just adding a few news sites but in prod we could add links to bitbucket, jira etc.

## console.nope

I renamed this from yaml because the behavior was quite annoying in test. It sets a URL to redirect to when the console logs out due to timeout.

## console-notification.yaml

Sets a handy message banner to notify console users of impending cluster work etc.

