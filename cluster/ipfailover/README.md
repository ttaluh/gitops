# IP Failover

---
## Addresses assigned in this example
* 192.168.20.15
* 192.168.20.16
* 192.168.20.17
* 192.168.20.18
* 192.168.20.19
* 192.168.20.20

# Manual process
Create a new project 

```
oc new-project ipfailover
```

And a new service account

```
oc create sa ipfailover
```

Update security context constraints (SCC) for hostNetwork:

```
oc adm policy add-scc-to-user privileged -z ipfailover
oc adm policy add-scc-to-user hostnetwork -z ipfailover
```

Add a new deployment

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ipfailover-keepalived 
  labels:
    ipfailover: hello-openshift
spec:
  strategy:
    type: Recreate
  replicas: 3
  selector:
    matchLabels:
      ipfailover: hello-openshift
  template:
    metadata:
      labels:
        ipfailover: hello-openshift
    spec:
      serviceAccountName: ipfailover
      privileged: true
      hostNetwork: true
      nodeSelector:
        node-role.kubernetes.io/worker: ""
      containers:
      - name: openshift-ipfailover
        image: registry.badger.local:5001/openshift4/ose-keepalived-ipfailover
        ports:
        - containerPort: 63000
          hostPort: 63000
        imagePullPolicy: IfNotPresent
        securityContext:
          privileged: true
        volumeMounts:
        - name: lib-modules
          mountPath: /lib/modules
          readOnly: true
        - name: host-slash
          mountPath: /host
          readOnly: true
          mountPropagation: HostToContainer
        - name: etc-sysconfig
          mountPath: /etc/sysconfig
          readOnly: true
        - name: config-volume
          mountPath: /etc/keepalive
        env:
        - name: OPENSHIFT_HA_CONFIG_NAME
          value: "ipfailover"
        - name: OPENSHIFT_HA_VIRTUAL_IPS 
          value: "192.168.20.10,192.168.20.11,192.168.20.12,192.168.20.13"
        - name: OPENSHIFT_HA_VIP_GROUPS 
          value: "10"
        - name: OPENSHIFT_HA_NETWORK_INTERFACE 
          value: "br-ex" #The host interface to assign the VIPs
        - name: OPENSHIFT_HA_MONITOR_PORT 
          value: "0"
        - name: OPENSHIFT_HA_VRRP_ID_OFFSET 
          value: "0"
        - name: OPENSHIFT_HA_REPLICA_COUNT 
          value: "3" #Must match the number of replicas in the deployment
        - name: OPENSHIFT_HA_USE_UNICAST
          value: "false"
        #- name: OPENSHIFT_HA_UNICAST_PEERS
          #value: "10.0.148.40,10.0.160.234,10.0.199.110"
        - name: OPENSHIFT_HA_IPTABLES_CHAIN 
          value: "INPUT"
        #- name: OPENSHIFT_HA_NOTIFY_SCRIPT 
        #  value: /etc/keepalive/mynotifyscript.sh
        #- name: OPENSHIFT_HA_CHECK_SCRIPT 
        #  value: "/etc/keepalive/mycheckscript.sh"
        - name: OPENSHIFT_HA_PREEMPTION 
          value: "preempt_delay 300"
        - name: OPENSHIFT_HA_CHECK_INTERVAL 
          value: "2"
        livenessProbe:
          initialDelaySeconds: 10
          exec:
            command:
            - pgrep
            - keepalived
      volumes:
      - name: lib-modules
        hostPath:
          path: /lib/modules
      - name: host-slash
        hostPath:
          path: /
      - name: etc-sysconfig
        hostPath:
          path: /etc/sysconfig
      # config-volume contains the check script
      # created with `oc create configmap keepalived-checkscript --from-file=mycheckscript.sh`
      - configMap:
          defaultMode: 0755
          name: keepalived-checkscript
        name: config-volume
      imagePullSecrets:
        - name: openshift-pull-secret 
```

You'll need to create a few nonsensical scripts as configmaps.

mycheckscript.sh:

```
#!/bin/bash
    # Whatever tests are needed
    # E.g., send request and verify response
exit 0
```

And make it a config map

```
oc create configmap mycustomcheck --from-file=mycheckscript.sh
```



# External IP
Configure the IP Ranges we want use for External IP

```
oc edit Network.config.openshift.io/cluster

Adding 

...
  externalIP:
    policy:
      allowedCIDRs:
      - 192.168.20.0/24
...
```


Now the Addresses specified should be added to the nodes covered by the selector.
You can now specify and address in a service.

```
apiVersion: v1
kind: Service
metadata:
  annotations:
    openshift.io/generated-by: OpenShiftWebConsole
  labels:
    app: httpd
    app.kubernetes.io/component: httpd
    app.kubernetes.io/instance: httpd
    app.kubernetes.io/name: httpd
    app.kubernetes.io/part-of: httpd-app
  name: httpd-external
  namespace: httpd-test
spec:
  externalIPs:
  - 192.168.20.13
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - name: 8080-tcp
    port: 8080
  - name: 8443-tcp
    port: 8443
  selector:
    app: httpd
    deploymentconfig: httpd
  type: LoadBalancer
```

