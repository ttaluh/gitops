# LDAP Authentication

Authenticating users on the console in openshift involves setting up two seperate processes. First we have the identify provider, then we need to set up the ldap group sync. Group sync is run via a cronjob from within the OCP cluster.

## LDAP Identify provider

### ../OAuth-cr.yaml

This is the file setting up the identify provider. This file includes all the identify providers, so for my cluster it also included the HTPASS provider.

The pertinent parts for ldap are 

```yaml
  - ldap:
      attributes:
        email:
        - mail
        id:
        - sAMAccountName
        name:
        - sAMAccountName
        preferredUsername:
        - sAMAccountName
      bindDN: CN=Administrator,CN=Users,DC=userdomain,DC=local
      bindPassword:
        name: ldap-bind-password
      ca:
        name: ldap-ca
      insecure: false
      url: ldaps://win-dc.userdomain.local/DC=userdomain,DC=local?sAMAccountName?sub
    mappingMethod: claim
    name: REF_LDAP
    type: LDAP
```

In prod we could use another attribute for name, I created my domain specifically for this test and so it wasn't very well populated. This config relies on a secret and a config map, these are defined in the following two files.

### ldap-bind-password-secret.yaml

Just a secret with the bind password for the domain in it.

### ldap-ca-configmap.yaml

A secret with the domains root certificate. My domain only has a root ca but we'll need to make this a bundle with the root and issuing CA's certs.

## LDAP Group Sync

### namespace.yaml

Creates the ldap-sync namespace

### ldap-sync-service-account.yaml

Creates the ldap-group-syncer service account.

### ldap-sync=cluster-role.yaml

Creates a new cluster role for ldap group sync called ldap-group-syncer with the permissions we need.

### ldap-sync-cluster-role-binding.yaml

Binds our service account to the ldap-group-syncer cluster role

### ldap-sync-secret.yaml
Here I've combined a few things. I've made the whole thing a secret since we're going to have for example out bind password in the config.

#### ca.crt
The domain certificate (same as for the indentity provider)

#### config.yaml
This is our main config file.

```yaml
kind: LDAPSyncConfig
apiVersion: v1
url: ldaps://win-dc.userdomain.local:636
bindDN: CN=Administrator,CN=Users,DC=userdomain,DC=local
bindPassword: C0mpaq01
insecure: false
ca: /ldap-sync/ca.crt
groupUIDNameMapping:
  "CN=cluster-admin,OU=Groups,OU=OpenShift,DC=userdomain,DC=local": ldap-cluster-admins
  "CN=cluster-user,OU=Groups,OU=OpenShift,DC=userdomain,DC=local": ldap-cluster-users
activeDirectory:
  usersQuery:
    baseDN: "OU=Users,OU=OpenShift,DC=userdomain,DC=local"
    scope: sub
    derefAliases: never
    filter: (objectclass=person)
    pageSize: 0
  userNameAttributes: [ sAMAccountName ]
  groupMembershipAttributes: [ memberOf ]
```

#### whitelist.txt

This is the whitelist for which groups to sync, we use this so we can limit the groups in the OU we're targeting to only those we care about.

### ldap-sync-cron-job.yaml

This defines the cron job to periodically run. We mount our secret in /ldap-sync so we know where to find the various files.


### ldap-group-cluster-admins-rolebinding.yaml

Here we create a role binding for our ldap-cluster-admins group (created by ldap-sync) to the cluster role cluster-admins. This is the only mapping I have but for prod we're likely to have several.
