# Mods here

## admin-role-binding.yaml

Can you guess? It adds a role binding making the amdin account a cluster-admin.

## htpass-secret.yaml

Adds the htpass file into a secret to be used by the identity provider

## ../OAuth-cr.yaml

This is the main auth config for our cluster. In my cluster that means we have the HTPASS and LDAP intentity providers set. We also have some token timeout config.
