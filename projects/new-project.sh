#!/bin/bash
set -e
# Run this with a parameter file to created the initial resources for a new project.

# vars
TEMPLATE="./project_template.yaml"
PROJECT_PATH="../cluster/projects/"


if [ $# -ne 1 ]; then
	echo "You need to provide a parameter file"
	echo "See params_example.txt"
	exit 5
fi

if [ ! -r $1 ]; then
    echo "$1 is not readable or does not exist"
    exit 7
fi    

PARAM="$1"
source ./$PARAM

tmpfile=$(mktemp)

if [ ! -r $TEMPLATE ]; then
    echo "$TEMPLATE is not readable or does not exist"
    exit 8
fi    

if [ ! -d $PROJECT_PATH ]; then
    echo "$PROJECT_PATH does not exist"
    exit 9
fi    



project_yaml=$(cat $TEMPLATE)

while read line; 
do 
	if [ ! -z "$line" ]
	then
		var=$(echo $line | cut -d= -f1)
		value=$(echo $line | cut -d= -f2 | sed 's/"//g')
		
		tmp=$(echo "$project_yaml" | sed "s|##$var|$value|g")
	        project_yaml=$tmp
		echo "$project_yaml"  > $tmpfile
                
	fi
done < $PARAM

mv $tmpfile ${PROJECT_PATH}${PROJECT_NAME}_project.yaml
echo "Project file has been created at ${PROJECT_PATH}${PROJECT_NAME}_project.yaml"
echo "Commit this to the repo to manage it"


rm -f ${tmpfile}
